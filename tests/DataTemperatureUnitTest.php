<?php

namespace App\Tests;

use App\Entity\DataTemperature;
use PHPUnit\Framework\TestCase;

class DataTemperatureUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $dataTemperature = new dataTemperature();
        $date1 = new \DateTime('2021-10-08 15:30:00');

        $dataTemperature  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidate(true);

        $this->assertTrue($dataTemperature->getDateHeure() === $date1);
        $this->assertTrue($dataTemperature->getValeur() === '5.00');
        $this->assertTrue($dataTemperature->getValidate() === true);
    }

    public function testIsFalse()
    {
        $dataTemperature = new dataTemperature();
        $date1 = new \DateTime('2021-10-08 15:30:00');
        $date2 = new \DateTime('2019-10-08 17:30:00');

        $dataTemperature  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidate(true);

        $this->assertFalse($dataTemperature->getDateHeure() === $date2);
        $this->assertFalse($dataTemperature->getValeur() === '1.00');
        $this->assertFalse($dataTemperature->getValidate() === false);
    }

    public function testIsEmpty()
    {
        $dataTemperature = new dataTemperature();

        $this->assertEmpty($dataTemperature->getDateHeure());
        $this->assertEmpty($dataTemperature->getValeur());
        $this->assertEmpty($dataTemperature->getValidate());
    }
}