<?php

namespace App\Tests;

use App\Entity\DataHygrometrie;
use PHPUnit\Framework\TestCase;


class DataHygrometrieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $dataHygrometrie = new dataHygrometrie();
        $date1 = new \DateTime('2021-10-08 15:30:00');

        $dataHygrometrie  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidate(true);

        $this->assertTrue($dataHygrometrie->getDateHeure() === $date1);
        $this->assertTrue($dataHygrometrie->getValeur() === '5.00');
        $this->assertTrue($dataHygrometrie->getValidate() === true);
    }

    public function testIsFalse()
    {
        $dataHygrometrie = new dataHygrometrie();
        $date1 = new \DateTime('2021-10-08 15:30:00');
        $date2 = new \DateTime('2019-10-08 17:30:00');

        $dataHygrometrie  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidate(true);

        $this->assertFalse($dataHygrometrie->getDateHeure() === $date2);
        $this->assertFalse($dataHygrometrie->getValeur() === '1.00');
        $this->assertFalse($dataHygrometrie->getValidate() === false);
    }

    public function testIsEmpty()
    {
        $dataHygrometrie = new dataHygrometrie();

        $this->assertEmpty($dataHygrometrie->getDateHeure());
        $this->assertEmpty($dataHygrometrie->getValeur());
        $this->assertEmpty($dataHygrometrie->getValidate());
    }
}