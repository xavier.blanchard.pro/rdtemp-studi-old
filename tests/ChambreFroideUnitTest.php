<?php

namespace App\Tests;

use App\Entity\ChambreFroide;
use PHPUnit\Framework\TestCase;

class ChambreFroideUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $chambreFroide = new chambreFroide();

        $chambreFroide  ->setName('ChambreFroide-001')
            ->setTempMin('1.00')
            ->setTempMax('9.00')
            ->setHygroMin('10.00')
            ->setHygroMax('90.00');

        $this->assertTrue($chambreFroide->getName() ==='ChambreFroide-001');
        $this->assertTrue($chambreFroide->getTempMin() === '1.00');
        $this->assertTrue($chambreFroide->getTempMax() === '9.00');
        $this->assertTrue($chambreFroide->getHygroMin() === '10.00');
        $this->assertTrue($chambreFroide->getHygroMax() === '90.00');
    }

    public function testIsFalse()
    {
        $chambreFroide = new chambreFroide();

        $chambreFroide  ->setName('ChambreFroide-001')
            ->setTempMin('1.00')
            ->setTempMax('9.00')
            ->setHygroMin('10.00')
            ->setHygroMax('90.00');

        $this->assertFalse($chambreFroide->getName() ==='ChambreFroide-002');
        $this->assertFalse($chambreFroide->getTempMin() === '2.00');
        $this->assertFalse($chambreFroide->getTempMax() === '8.00');
        $this->assertFalse($chambreFroide->getHygroMin() === '20.00');
        $this->assertFalse($chambreFroide->getHygroMax() === '80.00');
    }
    
    public function testIsEmpty()
    {
        $chambreFroide = new chambreFroide();

        $this->assertEmpty($chambreFroide->getName());
        $this->assertEmpty($chambreFroide->getTempMin());
        $this->assertEmpty($chambreFroide->getTempMax());
        $this->assertEmpty($chambreFroide->getHygroMin());
        $this->assertEmpty($chambreFroide->getHygroMax());
    }
}