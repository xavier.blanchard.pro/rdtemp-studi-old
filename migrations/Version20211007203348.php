<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007203348 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chambre_froide (id INT AUTO_INCREMENT NOT NULL, officine_id INT NOT NULL, name VARCHAR(255) NOT NULL, temp_min NUMERIC(10, 2) NOT NULL, temp_max NUMERIC(10, 2) NOT NULL, hygro_min NUMERIC(10, 2) NOT NULL, hygro_max NUMERIC(10, 2) NOT NULL, INDEX IDX_5E245AA7B2D03E4E (officine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_hygrometrie (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(10, 2) NOT NULL, validate TINYINT(1) NOT NULL, INDEX IDX_F25B70C621DF84 (chambre_froide_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_temperature (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(10, 2) NOT NULL, validate TINYINT(1) NOT NULL, INDEX IDX_418E7526C621DF84 (chambre_froide_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE officine (id INT AUTO_INCREMENT NOT NULL, raison_sociale VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, telephone VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chambre_froide ADD CONSTRAINT FK_5E245AA7B2D03E4E FOREIGN KEY (officine_id) REFERENCES officine (id)');
        $this->addSql('ALTER TABLE data_hygrometrie ADD CONSTRAINT FK_F25B70C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('ALTER TABLE data_temperature ADD CONSTRAINT FK_418E7526C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data_hygrometrie DROP FOREIGN KEY FK_F25B70C621DF84');
        $this->addSql('ALTER TABLE data_temperature DROP FOREIGN KEY FK_418E7526C621DF84');
        $this->addSql('ALTER TABLE chambre_froide DROP FOREIGN KEY FK_5E245AA7B2D03E4E');
        $this->addSql('DROP TABLE chambre_froide');
        $this->addSql('DROP TABLE data_hygrometrie');
        $this->addSql('DROP TABLE data_temperature');
        $this->addSql('DROP TABLE officine');
    }
}
