# RDtemp-Studi

Projet ECF du Graduate Développeur Web et Web Mobile STUDI

## Environement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* XAMPP
* nodejs et npm
* template Dashio


Vous pouvez vérifier les pré-requis avec la commande suivante (de la CLI-Symfony) :

***bash
symfony check:requirements
***

### Lancer l'environnement de développement

***bash
composer install
npm install
npm run build
symfony serve -d
***

### Lancer les tests unitaires

***bash
php bin.phpunit --testdox
***



# Credits

Xavier BLANCHARD - xavier.blanchard.pro@gmail.com