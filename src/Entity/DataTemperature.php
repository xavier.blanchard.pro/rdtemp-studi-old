<?php

namespace App\Entity;

use App\Repository\DataTemperatureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataTemperatureRepository::class)
 */
class DataTemperature
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateHeure;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $valeur;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validate;

    /**
     * @ORM\ManyToOne(targetEntity=chambreFroide::class, inversedBy="dataTemperatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chambreFroide;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->dateHeure;
    }

    public function setDateHeure(\DateTimeInterface $dateHeure): self
    {
        $this->dateHeure = $dateHeure;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getValidate(): ?bool
    {
        return $this->validate;
    }

    public function setValidate(bool $validate): self
    {
        $this->validate = $validate;

        return $this;
    }

    public function getChambreFroide(): ?chambreFroide
    {
        return $this->chambreFroide;
    }

    public function setChambreFroide(?chambreFroide $chambreFroide): self
    {
        $this->chambreFroide = $chambreFroide;

        return $this;
    }
}
