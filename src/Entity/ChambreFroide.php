<?php

namespace App\Entity;

use App\Repository\ChambreFroideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChambreFroideRepository::class)
 */
class ChambreFroide
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $tempMin;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $tempMax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $HygroMin;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $hygroMax;

    /**
     * @ORM\ManyToOne(targetEntity=officine::class, inversedBy="chambreFroides")
     * @ORM\JoinColumn(nullable=false)
     */
    private $officine;

    /**
     * @ORM\OneToMany(targetEntity=DataTemperature::class, mappedBy="chambreFroide")
     */
    private $dataTemperatures;

    /**
     * @ORM\OneToMany(targetEntity=DataHygrometrie::class, mappedBy="chambreFroide")
     */
    private $dataHygrometries;

    public function __construct()
    {
        $this->dataTemperatures = new ArrayCollection();
        $this->dataHygrometries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTempMin(): ?string
    {
        return $this->tempMin;
    }

    public function setTempMin(string $tempMin): self
    {
        $this->tempMin = $tempMin;

        return $this;
    }

    public function getTempMax(): ?string
    {
        return $this->tempMax;
    }

    public function setTempMax(string $tempMax): self
    {
        $this->tempMax = $tempMax;

        return $this;
    }

    public function getHygroMin(): ?string
    {
        return $this->HygroMin;
    }

    public function setHygroMin(string $HygroMin): self
    {
        $this->HygroMin = $HygroMin;

        return $this;
    }

    public function getHygroMax(): ?string
    {
        return $this->hygroMax;
    }

    public function setHygroMax(string $hygroMax): self
    {
        $this->hygroMax = $hygroMax;

        return $this;
    }

    public function getOfficine(): ?officine
    {
        return $this->officine;
    }

    public function setOfficine(?officine $officine): self
    {
        $this->officine = $officine;

        return $this;
    }

    /**
     * @return Collection|DataTemperature[]
     */
    public function getDataTemperatures(): Collection
    {
        return $this->dataTemperatures;
    }

    public function addDataTemperature(DataTemperature $dataTemperature): self
    {
        if (!$this->dataTemperatures->contains($dataTemperature)) {
            $this->dataTemperatures[] = $dataTemperature;
            $dataTemperature->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataTemperature(DataTemperature $dataTemperature): self
    {
        if ($this->dataTemperatures->removeElement($dataTemperature)) {
            // set the owning side to null (unless already changed)
            if ($dataTemperature->getChambreFroide() === $this) {
                $dataTemperature->setChambreFroide(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataHygrometrie[]
     */
    public function getDataHygrometries(): Collection
    {
        return $this->dataHygrometries;
    }

    public function addDataHygrometry(DataHygrometrie $dataHygrometry): self
    {
        if (!$this->dataHygrometries->contains($dataHygrometry)) {
            $this->dataHygrometries[] = $dataHygrometry;
            $dataHygrometry->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataHygrometry(DataHygrometrie $dataHygrometry): self
    {
        if ($this->dataHygrometries->removeElement($dataHygrometry)) {
            // set the owning side to null (unless already changed)
            if ($dataHygrometry->getChambreFroide() === $this) {
                $dataHygrometry->setChambreFroide(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
