<?php

namespace App\Repository;

use App\Entity\DataTemperature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataTemperature|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataTemperature|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataTemperature[]    findAll()
 * @method DataTemperature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataTemperatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataTemperature::class);
    }

    // /**
    //  * @return DataTemperature[] Returns an array of DataTemperature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataTemperature
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
