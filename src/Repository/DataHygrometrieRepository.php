<?php

namespace App\Repository;

use App\Entity\DataHygrometrie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataHygrometrie|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataHygrometrie|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataHygrometrie[]    findAll()
 * @method DataHygrometrie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataHygrometrieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataHygrometrie::class);
    }

    // /**
    //  * @return DataHygrometrie[] Returns an array of DataHygrometrie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataHygrometrie
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
